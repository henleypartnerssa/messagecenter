# Changelog

All Notable changes to MessageCenter will be documented in this file.

Updates should follow the principles.

## NEXT - YYYY-MM-DD

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing
