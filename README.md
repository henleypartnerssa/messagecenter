# MessageCenter

This modulus Laravel Specific Package for HP Online Sales application contains for functionality that acts as a Messaging Tool between the HP Online Sales
User Interface and the HP Online Sales Admin Panel.

## Install

Via Composer

``` bash
$ composer require hpsadev/MessageCenter
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.
