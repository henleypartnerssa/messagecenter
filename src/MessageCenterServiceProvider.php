<?php

namespace hpsadev\MessageCenter;

use Illuminate\Support\ServiceProvider;

class MessageCenterServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__.'/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/Views', 'messagecenter');

        $this->publishes([
            __DIR__.'/Views' => base_path("resources/views/vendor/messagecenter"),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
