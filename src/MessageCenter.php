<?php

namespace hpsadev\MessageCenter;

use Auth;

use App\User;
use App\Group;
use App\Message;

/**
 * This Package Class contains all main functionality reagrding
 * the Messaging tool between User Public and Admin interfaces.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.2
 */
class MessageCenter {

    /**
     * List all User messages.
     * @since  0.0.2 Function Init
     * @param  User $user User Object
     * @return array       User messages
     */
    public static function getUserMessages($user = NULL) {
        $array          = [];
        $subjects       = [];
        $messages       = ($user) ? $user->messages()->orderBy('created_at', 'desc')->get() : Auth::user()->messages()->orderBy('created_at', 'desc')->get();
        foreach ($messages as $i => $msg) {
            if ($i !== 0) {
                if (array_search($msg->subject, $subjects) !== false) {
                    continue;
                }
            }
            array_push($subjects, $msg->subject);
            $from   = User::find($msg->sent_from);
            $to     = User::find($msg->user_id);
            array_push($array,[
                'id'        => $msg->message_id,
                'subject'   => $msg->subject,
                'from'      => $from->name.' '.$from->surname,
                'to'        => $to->name.' '.$to->surname,
                'read'      => $msg->read,
                'timestamp' => $msg->created_at->toDateTimeString()
            ]);
        }
        return $array;
    }

    /**
     * Create Message when sent to Admin.
     * @since  0.0.2 Function Init
     * @param  POST $request POST data
     * @return Message          New Message
     */
    public static function sendAdminMessage($request) {
        $user       = Auth::user();
        $admin      = User::where('group_id', Group::where('roles', 'admin')->first()->group_id)->first();
        $message    = Message::create([
            'application_id'    => $request->app,
            'user_id'           => $admin->user_id,
            'group_id'          => $admin->group_id,
            'subject'           => $request->subject,
            'sent_from'         => $user->user_id,
            'read'              => false,
            'message'           => $request->message,
        ]);

        return $message;

    }

    /**
     * Create Reply message from Admin.
     * @since  0.0.2 Function Init
     * @param  integer $id      Message ID
     * @param  POST $request POST Data
     * @return Message          Reply Message
     */
    public static function replyAdminMessage($id,$request) {
        $message    = Message::find($id);
        $user       = User::find($message->sent_from);
        $newMsg     = Message::create([
            'application_id'    => $message->application_id,
            'user_id'           => $user->user_id,
            'group_id'          => $user->group_id,
            'subject'           => $message->subject,
            'sent_from'         => $message->user_id,
            'read'              => false,
            'message'           => $request->message,
        ]);
        return $newMsg;
    }

}
