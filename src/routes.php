<?php

Route::get('/admin/messaging/messages', 'hpsadev\MessageCenter\Controllers\MessagingController@adminMessages');
Route::get('/admin/messaging/user-messages/{profileID}', 'hpsadev\MessageCenter\Controllers\MessagingController@adminUserMessages');
Route::get('/admin/messaging/message/{id}', 'hpsadev\MessageCenter\Controllers\MessagingController@adminViewMessage');
Route::post('/admin/messaging/message/{id}', 'hpsadev\MessageCenter\Controllers\MessagingController@replyAdminMessage');

Route::get('/users/messaging/messages', 'hpsadev\MessageCenter\Controllers\MessagingController@userMessages');
Route::get('/users/messaging/create', 'hpsadev\MessageCenter\Controllers\MessagingController@userCreateMessage');
Route::get('/users/messaging/message/{id}', 'hpsadev\MessageCenter\Controllers\MessagingController@userViewMessage');
Route::post('/users/messaging/message/{id}', 'hpsadev\MessageCenter\Controllers\MessagingController@replyUserMessage');
Route::post('/users/messaging/create', 'hpsadev\MessageCenter\Controllers\MessagingController@sendAdminMessage');
