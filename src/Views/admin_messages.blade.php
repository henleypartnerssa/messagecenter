@extends('nladminpanel::admin_app')

@section('title', 'Dashboard')

@section('content')

<h3 class="ui center aligned block header">
    Your Inbox Messages
</h3>

<div class="ui middle aligned selection list">
    @if($listMessages)
        @foreach($listMessages as $message)
        <a class="item" style="text-align: left" id="{{$message['id']}}" href="/admin/messaging/message/{{$message['id']}}">
            <div class="right floated content">
                <small style="float:right"><b>{{$message['timestamp']}}</b></small>
            </div>
            @if($message['read'] === false)
                <i class="ui avatar image mail icon"></i>
            @else
                <i class="ui avatar image mail outline icon"></i>
            @endif
            <div class="content">
                <div class="header">From: {{$message['from']}}</div>
                <div class="description">Subject: {{$message['subject']}}</div>
            </div>
        </a>
        @endforeach
    @endif
</div>

@stop
