<?php

namespace hpsadev\MessageCenter\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use hpsadev\MessageCenter\MessageCenter;

use App\User;
use App\Application;
use App\Message;
use App\Profile;

class MessagingController extends Controller
{

    public function __construct() {
        $this->middleware('nopin');
    }

    public function userMessages() {
        $listMessages = MessageCenter::getUserMessages();
        return view('messagecenter::messages', compact('listMessages'));
    }

    public function userCreateMessage() {
        $app    = Auth::user()->profiles()->first()->application_id;
        return view('messagecenter::create', compact('app'));
    }

    public function sendAdminMessage(Request $request) {
        MessageCenter::sendAdminMessage($request);
        return redirect('/users/messaging/messages');
    }

    public function replyAdminMessage($id, Request $request) {
        MessageCenter::replyAdminMessage($id, $request);
        return redirect('/admin/messaging/messages');
    }

    public function replyUserMessage($id, Request $request) {
        MessageCenter::replyAdminMessage($id, $request);
        return redirect('/users/messaging/messages');
    }

    public function adminMessages() {
        $listMessages = MessageCenter::getUserMessages();
        return view('messagecenter::admin_messages', compact('listMessages'));
    }

    public function adminUserMessages($profileID) {
        $profile        = Profile::find($profileID);
        $user           = $profile->user;
        $listMessages   = MessageCenter::getUserMessages($user);
        return view('messagecenter::admin_user_messages', compact('listMessages', 'profile'));
    }

    public function adminViewMessage($messageId) {
        $message        = Message::findOrFail($messageId);
        $user           = $message->user;
        $group          = $user->group->roles;
        $profile        = ($group !== 'admin') ? $user->profiles()->where('belongs_to', 'Principal Applicant')->first() : NULL;
        $message->read  = true;
        $message->save();
        $prevMessages   = Message::where('subject', $message->subject)->orderBy('created_at', 'asc')->get();
        $view           = ($group !== 'admin') ? 'admin_user_view' : 'admin_view';
        return view('messagecenter::'.$view, compact('message', 'prevMessages', 'profile'));
    }

    public function userViewMessage($messageId) {
        $message        = Message::findOrFail($messageId);
        $message->read  = true;
        $message->save();
        $prevMessages   = Message::where('subject', $message->subject)->orderBy('created_at', 'asc')->get();
        return view('messagecenter::view', compact('message', 'prevMessages'));
    }

}
